<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Route::get('/admin', function () {
// 	if(Gate::allows('admin-panel', Auth::user())){
// 		return view('admin.admin-panel');
// 	}else{
// 		return 'not authorized to view this page';
// 	}
// })->name('admin');
// 
	Route::get('/pharmacy', 'PharmacyController@index')->name('pharmacy');
	Route::get('/admin', 'AdminController@index')->name('admin');

	Route::get('/sales', 'SalesController@index')->name('sales');
	Route::post('/admin/assign-menu', 'AdminController@assignMenuToRole')->name('role.assign-menu');
		

	


