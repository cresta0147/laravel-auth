<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class PharmacyController extends Controller
{
   public function index(){
   	if($this->authorize('admin-panel', Auth::user())){
   		return view('pharmacy.pharmacyPanel');
   	}else{
   		return 'not authorized';
   	}
   }
}
