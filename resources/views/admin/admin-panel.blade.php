@extends ('layouts.app')

@section('content')
	<div class="container">
    	<div class="row">
    		welcome admin
    	</div>
    	<div class="row">
            
        		<table>
        			<tr>
        				<th>Role</th>
        				@foreach ($menus as $menu)
        					<th>{{$menu->name}}</th>
        				@endforeach
        			</tr>
        			@foreach ($roles as $role)
                	   <tr>

                            <form action="{{route('role.assign-menu')}}" method="POST">      
                    			<td>{{$role->name}}</td>
                                <input type="hidden" name="role_id" value="{{$role->id}}">
                    			@for ($i = 0; $i <count($menus) ; $i++)
                    				<td><input type="checkbox" name="menu_{{$menus[$i]->name}}" {{ $role->canAccessMenu($menus[$i]->name)?'checked' : ''}}>
                                    </td>
                                     {{-- {{ csrf_field() }} --}}
                                    
                                     
                    			@endfor
                                 <td>
                                     <button type="submit" name="submit">Assign Menu to Roles</button>
                                 </td>
                               
                                   
                            </form>
                           
                		</tr>
                       
        			@endforeach
        			
        		</table>

                
          
    	</div>
    </div>
@endsection