<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Menu;
use App\Role;
class AdminController extends Controller
{
    public function index(){
    	if($this->authorize('admin-panel', Auth::user())){
    		$menus = Menu::all();
    		$roles = Role::all();
    		return view('admin.admin-panel', ['menus' => $menus, 'roles' => $roles]);
    	}else{
    		return  'not authorized';
    	}
    }

    public function assignMenuToRole(Request $request){
    	var_dump ($_POST);
        die();
    }
}
