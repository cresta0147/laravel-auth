<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function index(){
    	
    	if($this->authorize('sales-access', Auth::user())){
    		return view('sales.salesPanel');
    	}else{
    		return 'not authrized';
    	}
    }
}
