<?php

namespace App\Providers;
use App\User;
use App\Policies\AdminPolicy;
use App\Policies\SalesPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => AdminPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-panel', 'AdminPolicy@adminPanel');

        Gate::define('sales-access', function($user){
            return $user->authorizeMenu($user, 'sales');
        });
         Gate::define('pharmacy-access', function($user){
            return $user->authorizeMenu($user, 'pharmacy');
        });
    }
}
